package nano;


public class NanoUUID {


    public NanoUUID(String string, boolean log) {
        if (log)
            log("UUID=" + string);
        build(string, log);
    }

    byte[] mask; // <------------- This is where the magic will happens

    private final void build(final String id, boolean log) {
        char[] chars = id.toCharArray();
        int length = id.length();
        int totalBits = length * 6; // each char occupies 6 bits
        int totalBytes = totalBits / 8;
        this.mask = new byte[totalBytes];
        if (log) {
            log("length=" + length + " mask size=" + totalBytes + " bytes for " + totalBits + " bits");
        }
        int bitposition = 0;
        for (char c : chars) {
            byte reducedByte = expandedToReduced((byte) c);
            setBits(bitposition, c, reducedByte,log);
            bitposition += SIZE;
        }
    }

    private byte expandedToReduced(byte b) {
        byte reducedByte;
        if (b == 45) {
            reducedByte = 36; //
        } else if ((b >= 48) && (b <= 57)) {
            reducedByte = (byte) (b - 48);
        } else {
            reducedByte = (byte) (b - 97 + 10);
        }
        return reducedByte;
    }


    private static int SIZE = 6;

    private void setBits(int maskPosition, char originalChar, byte reducedByte, boolean log) {
        int byteNumber = maskPosition / 8;
        int nextByte = byteNumber + 1;
        int start = maskPosition % 8;
        int last = calcLast(start);
        boolean useNeighbor = start + SIZE > 8;
        String extraMsg = "";
        int remaining = calcRemaining(start);
        boolean bits[] = FlagMaskUtil.getBits(reducedByte, 0, SIZE - 1);

        int c = 0;
        for (int i = start; i <= last; i++) {
            mask[byteNumber] = FlagMaskUtil.setBit(mask[byteNumber], i, bits[c]);
            c++;
        }
        if (useNeighbor) {
            for (int i = 0; i < remaining; i++) {
                mask[nextByte] = FlagMaskUtil.setBit(mask[nextByte], i, bits[c]);
                c++;
            }
        }
        if (log) {
            // output, for illustration
            if (useNeighbor) {
                extraMsg = " and #" + (nextByte) + " 0..." + (remaining - 1);
            }
            log("writing : char " + originalChar + " reduced=" + reducedByte + "\t   Byte #" + byteNumber + " " + start + "..." + last + extraMsg);
        }

    }

    private int calcRemaining(int start) {
        return start + SIZE - 8;
    }


    private String entireMask() {
        String s = "";
        for (int i = mask.length - 1; i >= 0; i--) {
            s += " " + FlagMaskUtil.byteToBitRepresentation(mask[i]);
        }
        return s;
    }


    private final String unbuild(boolean log) {
        int size = this.mask.length * 8;
        String result = "";
        int maskPosition = 0;
        int numberOfChars = mask.length * 8 / 6;
        if (log) {
            log("Reconstructing UUID of " + numberOfChars + " chars");
        }
        for (int k = 0; k < numberOfChars; k++) {
            int byteNumber = maskPosition / 8;
            int nextByte = byteNumber + 1;
            int start = maskPosition % 8;
            boolean useNeighbor = start + SIZE > 8;
            int last = calcLast(start);
            int remaining = calcRemaining(start);
            String xmsg = useNeighbor ? " and #" + (byteNumber + 1) + " 0..." + (remaining - 1) : "";

            boolean[] mainStored = FlagMaskUtil.getBits(mask[byteNumber], start, last);
            boolean[] complement = null;
            if (useNeighbor) {
                complement = FlagMaskUtil.getBits(mask[nextByte], 0, remaining - 1);
            }
            byte reconstructedReduced = FlagMaskUtil.composeBits(mainStored, complement);
            byte expanded = reducedToExpanded(reconstructedReduced);
            if (log) {
                log("Reconstructing reduced=" + reconstructedReduced + " code=" + expanded + " char=" + ((char) expanded) + "  \t from #" + byteNumber + " " + start + "..." + last + " " + xmsg);
            }
            result += (char) expanded;
            maskPosition += SIZE;
        }
        return result;
    }

    private byte reducedToExpanded(byte reconstructedReduced) {
        byte expanded;
        if (reconstructedReduced == (byte) 36) {
            expanded = 45;
        } else if (reconstructedReduced < 10) {
            expanded = (byte) (reconstructedReduced + 48);
        } else {
            expanded = (byte) (reconstructedReduced - 10 + 97);
        }
        return expanded;
    }

    private int calcLast(int start) {
        int last;
        if (start + SIZE > 8) {
            last = 7;
        } else {
            last = start + (SIZE - 1);
        }
        return last;
    }

    public String get(boolean log) {
        return unbuild(log);
    }

    private void log(String msg) {
        System.out.println(msg);
    }

}
