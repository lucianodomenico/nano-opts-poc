package nano;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Nano {
    NanoCollection nanoSmall;
    NanoCollection nanoMedium;
    NanoCollection nanoBig;
    StringCollection stringSmall;
    StringCollection stringMedium;
    StringCollection stringBig;

    public static void main(String[] args) {
        String par1 = args[0];
        String par2 = args[1];
        String par3 = args[2];
        String par4 = args[3];
        boolean useNano = "nano".equalsIgnoreCase(par1);
        int smallSize = Integer.parseInt(par2);
        int mediumSize = Integer.parseInt(par3);
        int bigSize = Integer.parseInt(par4);
        System.out.println("Parameters provided : useNano="+useNano+" smallSize="+smallSize+" mediumSize="+mediumSize+" bigSize="+bigSize);
        Nano nano = new Nano();
        nano.run(useNano,smallSize,mediumSize,bigSize);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("Ending NanoOpts test");
            }
        });
        try {
            Thread.currentThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void run(boolean useNano, int smallSize, int mediumSize, int bigSize) {
        log("-------------- Phase 1 - Creation -----------");
        if (useNano) {
            nanoSmall = new NanoCollectionSmall(smallSize);
            nanoMedium = new NanoCollectionMedium(mediumSize);
            nanoBig = new NanoCollectionBig(bigSize);
            holdInto(nanoSmall);
            holdInto(nanoMedium);
            holdInto(nanoBig);
            log("-------------- Phase 2 - Reading/Writing -----------");
            testReadPerformance(nanoSmall);
            testReadPerformance(nanoMedium);
            testReadPerformance(nanoBig);
        } else {
            stringSmall = new StringCollectionSmall(smallSize);
            stringMedium = new StringCollectionMedium(mediumSize);
            stringBig = new StringCollectionBig(bigSize);
            holdStringsInto(stringSmall);
            holdStringsInto(stringMedium);
            holdStringsInto(stringBig);
            log("-------------- Phase 2 - Reading/Writing -----------");
            testReadStringPerformance(stringSmall);
            testReadStringPerformance(stringMedium);
            testReadStringPerformance(stringBig);
        }
        log("-------------- Go ahead, use the profiler. CTRL-C to finish it.");
    }


    private void holdInto(NanoCollection holder) {
        log("Creating UUID collection using NanoUUID size=" + holder.getSize());
        long t0 = System.currentTimeMillis();
        List<NanoUUID> list = new ArrayList<>();
        for (int i = 0; i < holder.getSize(); i++) {
            String uuid = UUID.randomUUID().toString();
            list.add(new NanoUUID(uuid,false));
        }
        holder.hold(list);
        long t1 = System.currentTimeMillis();
        log("Spent " + (t1 - t0) + " ms");
    }

    private void holdStringsInto(StringCollection holder) {
        log("Creating UUID collection using String size=" + holder.getSize());
        long t0 = System.currentTimeMillis();
        List<String> list = new ArrayList<>();
        for (int i = 0; i < holder.getSize(); i++) {
            String uuid = UUID.randomUUID().toString();
            list.add(uuid);
        }
        holder.hold(list);
        long t1 = System.currentTimeMillis();
        log("Spent " + (t1 - t0) + " ms");
    }


    private void testReadPerformance(NanoCollection holder) {
        log("Testing reading " + holder.getSize() +" UUIDs");
        long t0 = System.currentTimeMillis();
        int t = 0;
        for (NanoUUID a : holder.getList()) {
            String s = a.get(false);
            int x = s.length();
            t = t + x;
        }
        long t1 = System.currentTimeMillis();
        log("Spent " + (t1 - t0) + " ms");
    }

    private void testReadStringPerformance(StringCollection holder) {
        log("Testing reading " + holder.getSize() +" UUIDs");
        long t0 = System.currentTimeMillis();
        int t = 0;
        for (String s : holder.getList()) {
            int x = s.length();
            t = t + x;
        }
        long t1 = System.currentTimeMillis();
        log("Spent " + (t1 - t0) + " ms");
    }


    private void log(String msg) {
        System.out.println("   " + msg);
    }


    //-=======================================

    class NanoCollectionSmall implements NanoCollection {
        NanoCollectionSmall(int size) {
            this.size = size;
        }

        int size;
        List<NanoUUID> list;

        public void hold(List<NanoUUID> alist) {
            list = new ArrayList<>();
            list.addAll(alist);
        }

        public List<NanoUUID> getList() {
            return list;
        }

        public int getSize() {
            return size;
        }
    }

    class NanoCollectionMedium implements NanoCollection {
        NanoCollectionMedium(int size) {
            this.size = size;
        }

        int size;
        List<NanoUUID> list;

        public void hold(List<NanoUUID> alist) {
            list = new ArrayList<>();
            list.addAll(alist);
        }

        public List<NanoUUID> getList() {
            return list;
        }

        public int getSize() {
            return size;
        }
    }

    class NanoCollectionBig implements NanoCollection {
        NanoCollectionBig(int size) {
            this.size = size;
        }

        int size;
        List<NanoUUID> list;

        public void hold(List<NanoUUID> alist) {
            list = new ArrayList<>();
            list.addAll(alist);
        }

        public List<NanoUUID> getList() {
            return list;
        }

        public int getSize() {
            return size;
        }
    }

    interface NanoCollection {
        void hold(List<NanoUUID> list);

        List<NanoUUID> getList();

        int getSize();
    }


//=== The version using STrings


    class StringCollectionSmall implements StringCollection {
        StringCollectionSmall(int size) {
            this.size = size;
        }

        int size;
        List<String> list;

        public void hold(List<String> alist) {
            list = new ArrayList<>();
            list.addAll(alist);
        }

        public List<String> getList() {
            return list;
        }

        public int getSize() {
            return size;
        }
    }

    class StringCollectionMedium implements StringCollection {
        StringCollectionMedium(int size) {
            this.size = size;
        }

        int size;
        List<String> list;

        public void hold(List<String> alist) {
            list = new ArrayList<>();
            list.addAll(alist);
        }

        public List<String> getList() {
            return list;
        }

        public int getSize() {
            return size;
        }
    }

    class StringCollectionBig implements StringCollection {
        StringCollectionBig(int size) {
            this.size = size;
        }

        int size;
        List<String> list;

        public void hold(List<String> alist) {
            list = new ArrayList<>();
            list.addAll(alist);
        }

        public List<String> getList() {
            return list;
        }

        public int getSize() {
            return size;
        }
    }

    interface StringCollection {
        void hold(List<String> list);

        List<String> getList();

        int getSize();
    }


}