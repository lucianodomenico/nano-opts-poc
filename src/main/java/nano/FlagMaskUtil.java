package nano;

public class FlagMaskUtil {

    public static byte setBit(byte mask, int position, boolean value) {
        byte result;
        if (value) {
            result = (byte) (mask | ((byte) 1 << position));
            return result;
        } else {
            result = (byte) (mask & ~(1 << position));
        }
        return result;
    }


    public static boolean getBit(byte mask, int position) {
        return (mask & (1 << position)) != 0;
    }

    /**
     * IT WILL RETURN THE array of boolean from right to left, so
     * 000101 will return (true,false,true,false,false,false)
     *
     * @param mask
     * @param start
     * @param end
     * @return
     */
    public static boolean[] getBits(byte mask, int start, int end) {
        boolean[] result = new boolean[end - start + 1];
        int c = 0;
        for (int i = start; i <= end; i++) {
            result[c++] = getBit(mask, i);
        }
        return result;
    }

    public static byte composeBits(boolean[]... masks) {
        byte result = 0;
        int c = 0;
        for (boolean[] mask : masks) {
            if (mask != null) {
                for (int i = 0; i < mask.length; i++) {
                    result = FlagMaskUtil.setBit(result, c, mask[i]);
                    c++;
                }
            }
        }
        return result;
    }


    public static String byteToBitRepresentation(byte b){
        return String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ', '0');
    }

}
