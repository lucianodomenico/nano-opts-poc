package nano;

import org.assertj.core.api.WithAssertions;
import org.junit.Test;

public class FlagMaskUtilTest implements WithAssertions {


    // THE BITMASK IS READ FROM RIGHT TO LEFT
    // 00000001  <- THE RIGHT MOST IF THE BIT ZERO (POSITION 0)
    // 10000000  <- THE LEFT MOST IF THE BIT SEVEN (POSITION 7)
    @Test
    public void setBit_setBitZero() { // 00000001
        // arrange
        byte mask = (byte) 0;
        // act
        byte modified = FlagMaskUtil.setBit(mask, 0, true);
        // assert
        assertThat(modified).isEqualTo((byte) 1);
    }

    @Test
    public void setBit_setBitOne() { // 00000010
        // arrange
        byte mask = (byte) 0;
        // act
        byte modified = FlagMaskUtil.setBit(mask, 1, true);
        // assert
        assertThat(modified).isEqualTo((byte) 2);
    }

    @Test
    public void setBit_setBitTwo() { // 00000100
        // arrange
        byte mask = (byte) 0;
        // act
        byte modified = FlagMaskUtil.setBit(mask, 2, true);
        // assert
        assertThat(modified).isEqualTo((byte) 4);
    }

    @Test
    public void setBit_setBitThreeAndFour() { // 00011000
        // arrange
        byte mask = (byte) 0;
        // act
        byte modified1 = FlagMaskUtil.setBit(mask, 3, true);
        byte modified2 = FlagMaskUtil.setBit(modified1, 4, true);
        // assert
        assertThat(modified2).isEqualTo((byte) 24);
    }


    @Test
    public void getBit_set3rdAndReadIt() {
        // arrange
        byte mask = (byte) 0;
        // act
        byte modified = FlagMaskUtil.setBit(mask, 3, true);
        boolean result = FlagMaskUtil.getBit(modified, 3);
        // assert
        assertThat(result).isTrue();
    }

    @Test
    public void getBit_set3rdAndSetItBackToNegative() {
        // arrange
        byte mask = (byte) 64; // 01000000 -> 0101000 -> 0100000
        // act
        byte intermediate = FlagMaskUtil.setBit(mask, 3, true);
        byte restored = FlagMaskUtil.setBit(intermediate, 3, false);
        // assert
        assertThat(intermediate).isEqualTo((byte) 72);
        assertThat(restored).isEqualTo((byte) 64);
    }

    @Test
    public void getBit_setOneBitAtATime() {
        // arrange
        byte mask = (byte)0; // 00000000
        // act
        for (int i =0; i <8; i++) {
            byte modified = FlagMaskUtil.setBit(mask, i, true);
            byte restored = FlagMaskUtil.setBit(mask, i, false);
//            System.out.println("modified #"+i+" to "+modified+" then restored to "+restored);
        }
        // assert
        assertThat(mask).isEqualTo((byte)0);
    }

    @Test
    public void getBit_set3rdAndReadOther_shouldBeFalse() {
        // arrange
        byte mask = (byte) 0;
        // act
        byte modified1 = FlagMaskUtil.setBit(mask, 3, true);
        boolean result = FlagMaskUtil.getBit(modified1, 4);
        // assert
        assertThat(result).isFalse();
    }


    @Test
    public void getBit_setBit7AndAnotherOneReadOther_shouldNotAffectBit7() {
        // arrange
        byte mask = (byte) -128;
        // act
        byte modified = FlagMaskUtil.setBit(mask, 3, true);
        boolean result = FlagMaskUtil.getBit(mask, 7);
        // assert
        assertThat(modified).isEqualTo((byte)-120);
        assertThat(result).isTrue();
    }

    @Test
    public void getBits() {
        // arrange
        byte mask = (byte) 19; // 10011
        // act
        boolean[] bits = FlagMaskUtil.getBits(mask, 0, 4);
        // assert
        assertThat(bits[0]).isTrue();
        assertThat(bits[1]).isTrue();
        assertThat(bits[2]).isFalse();
        assertThat(bits[3]).isFalse();
        assertThat(bits[4]).isTrue();
    }

    @Test
    public void composeBits_oneOneArray() {
        // assert
        boolean[] booleans = {true, true, false, false, false, true};
        boolean[] other = null;
        // act
        byte result = FlagMaskUtil.composeBits(booleans,other);
        // assert
        assertThat(result).isEqualTo((byte) 35);
    }
    @Test
    public void composeBits_twoArrays() {
        // assert
        boolean[] booleans = {true, true, false, false, false, true};
        boolean[] other = {true,false};
        // act
        byte result = FlagMaskUtil.composeBits(booleans,other);
        // assert
        assertThat(result).isEqualTo((byte) 99);
    }


    @Test
    public void testByteToBitRepresetation(){
        // arrange
        // act
        String s0  = FlagMaskUtil.byteToBitRepresentation((byte)0);
        String s4  = FlagMaskUtil.byteToBitRepresentation((byte)4);
        String s45 = FlagMaskUtil.byteToBitRepresentation((byte)45);
        String s80 = FlagMaskUtil.byteToBitRepresentation((byte)80);
        String n56 = FlagMaskUtil.byteToBitRepresentation((byte)-56);
        // assert
        assertThat(s0) .isEqualTo("00000000");
        assertThat(s4) .isEqualTo("00000100");
        assertThat(s45).isEqualTo("00101101");
        assertThat(s80).isEqualTo("01010000");
        assertThat(n56).isEqualTo("11001000");
    }
}
