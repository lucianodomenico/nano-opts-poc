package nano;

import org.assertj.core.api.WithAssertions;
import org.junit.Test;



public class NanoTest implements WithAssertions {


    @Test
    public void givenUUID_testConstructor() {
        // assert
        String UUID = java.util.UUID.randomUUID().toString();
        // act
        NanoUUID id = new NanoUUID(UUID,true);
        // assert
        String stringId = id.get(true);
        assertThat(stringId).isEqualTo(UUID);
    }


}